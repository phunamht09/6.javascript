// có 2 cách dom
const UBER_CAR = 'uberCar';
const UBER_SUV = 'uberSUV';
const UBER_BLACK = 'uberBlack';


var btnTinh = document.getElementById('btn-tinh-tien');

function tinhGiaTienKmDauTien(loaiXe) {
    if (loaiXe == UBER_CAR) {
        return 8000;
    }
    if (loaiXe = UBER_SUV) {
        return 9000;
    }
    if (loaiXe = UBER_BLACK) {
        return 10000;
    }
}

function tinhgiaTienKm1Den19(car) {
    if (car == UBER_CAR) {
        return 7500;
    }
    if (car = UBER_SUV) {
        return 8500;
    }
    if (car = UBER_BLACK) {
        return 9500;
    }
}

function tinhGiaTienKm19TroDi(loaiXe) {
    switch (loaiXe) {
        case UBER_CAR: {
            return 7000;
        }
        case UBER_SUV: {
            return 8000;
        }
        case UBER_BLACK: {
            return 9000;
        }
    }
}


// dùng DOM cách này phải có nút button trong html thì mới dùng được addEventListener
btnTinh.addEventListener('click', function () {
    var carOptionEl = document.querySelector('input[name="selector"]:checked').value;
    if (carOptionEl == null) {
        return;  //dừng chương trình nếu đã chọn 1 loại
    }
    var car = carOptionEl.value;
    var km = document.getElementById('txt-km').value * 1;
    var giaKmDautien = tinhGiaTienKmDauTien(car);
    var giaTienKm1Den19 = tinhgiaTienKm1Den19(car);
    var giaTienKm19TroDi = tinhGiaTienKm19TroDi(car);

    var tongTien;
    if (km <= 1) {
        tongTien = giaKmDautien * km;
    } else if (km <= 19) {
        tongTien = giaKmDautien * 1 + (km - 1) * giaTienKm1Den19;
    } else {
        tongTien = giaKmDautien * 1 + 18 * giaTienKm1Den19 + (km - 19) * giaTienKm19TroDi;
    }

    document.getElementById('divThanhTien').style.display = 'block';
    document.getElementById('divThanhTien').innerText = 'Thành tiền : ' + tongTien + 'VND';




    console.log('giaKmDautien');
});