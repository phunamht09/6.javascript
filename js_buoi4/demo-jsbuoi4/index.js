//JS BUỔI 4
// TOÁN TỬ LUẬN LÝ
var a = 5;
var b = 10;
console.log('a =', a, ';', 'b =', b);
console.log('Lớn hơn:', a>b);
console.log('Bé hơn:', a<b);
console.log('Bằng:', a == b);
console.log('Khác:', a != b);
console.log('Lớn hơn bằng:', a >= b);
console.log('Bé hớn bằng:', a <= b);

var d = 7; var e = '7';    //d: number  ; e: string
console.log('d == e', d == e);  //true vì ko phân biệt number vs string

console.log('d === e', d === e);  //false vì khi so sánh có tính đến kiểu loại dữ liệu, d là number, e là string nên sẽ không bằng nhau

//TOÁN TỬ SO SÁNH - LOGIC   
//not: phủ định
// A && B : đúng khi tất cả phải đúng
// A || B : đúng khi 1 trong 2 đúng
// !A  : phủ định của A
var dk1 = true;
var dk2 = true;
var dk3 = false;
console.log('dk1 =', dk1);
console.log('dk2 =', dk2);
console.log('dk1 && dk2 && dk3', dk1 && dk2 && dk3); // 1 trong 3 dk false thì kết quả false
console.log('dk1 || dk2 || dk3', dk1 || dk2 || dk3); // 1 trong 3 dk true thì kết quả true
console.log('!dk1', !dk1);  // phủ định của dk1
console.log('!!dk1', !!dk1);  //phủ định của phủ định dk1
 

//CẤU TRÚC RẼ NHÁNH TRONG LẬP TRÌNH: IF
if (d < 0) {
    d = -d;
}

//ví dụ 1: tính giá trị tuyệt đổi
document.getElementById('btnTinhTriTuyetDoi').onclick = function() {
    //input: số : number
    var iSo = document.getElementById('iSo_1').value;

    //output: số : number
    var ketQua = 0;

    //progress
    ketQua = iSo; //ví dụ: iSo = -5
    if(ketQua < 0) {
        ketQua = -iSo; //ket qua: 5
    }

    document.getElementById('ketQuaViDu1').innerHTML = ketQua;
}

//CẤU TRÚC RẼ NHÁNH TRONG LẬP TRÌNH: IF ELSE
// Đúng thì xử lý trường hợp 1
// Sai thì xử lý trường hợp 2

//Ví dụ 2: Viết chương trình cho phép người dùng nhập vào 1 số => In ra màn hình cho biết số đó là số chẵn hay lẻ

document.getElementById('btnKiemTraSo').onclick = function() {
    //input: number
    var iSo = document.getElementById('iSo_2').value;

    //output: string
    var ketQua = '';   //string

    //progress
    if (iSo % 2 == 0) {  // số chẵn: chia hết cho 2 và số dư là 0
        ketQua = 'Số chẵn';
    }else {
        ketQua = 'Số lẻ';
    }

    //hiển thị ra màn hình
    document.getElementById('ketQuaViDu2').innerHTML = ketQua;
}

//Ví dụ 3: Cho người dùng nhập vào 2 số. Tìm số lớn nhất và in ra kết quả

document.getElementById('btnTimSoLonNhat').onclick = function() {
    //input: number ?? string ?? boolean ??
    var soThuNhat = Number(document.getElementById('soThuNhat').value);
    var soThuHai = Number(document.getElementById('soThuHai').value);
    //Number cần được thêm vào để hệ thống biết được đó là dạng số


    //progress
    var max = soThuNhat;   //khai báo giả sử số thứ 1 là lớn nhất

    if (max < soThuHai) {  
        max = soThuHai;      //kỹ thuật đặt lính canh, cờ hiệu, đặt biến tạm
    }

    

    //output: number ?? string ?? boolean ??
    var ketQua = max;   //string

    //hiển thị kết quả ra màn hình
    document.getElementById('ketQuaViDu3').innerHTML = 'Số lớn nhất là:' + ketQua;
}

//Ví dụ 4: Viết chương trình cho phép người dùng nhập vào số giờ làm và tiền công 1 giờ. Yêu cầu: tính tiền công dựa trên số giờ làm theo công thức sau

document.getElementById('btnTinhTien').onclick = function() {
    //input: number ? string ?
    var soGioLamTrenTuan = Number(document.getElementById('soGioLamTrenTuan').value);
    var tienCongTrenGio = Number(document.getElementById('tienCongTrenGio').value);

    //output: number
    var tienLuong = 0;

    //Xét số giờ nhập vào xem có > 40 hay không
    if (tienCongTrenGio > 40) {
        //khai báo cho số lượng giờ > 40
        var soGioSau = soGioLamTrenTuan - 40; //số giờ này để tính 1.5
        var tienTangCa = soGioSau * (tienCongTrenGio * 1.5);
        tienLuong = (40 * tienCongTrenGio) + tienTangCa;
    }else {
        //ngược lại nếu <= 40 giờ
        tienLuong = soGioLamTrenTuan * tienCongTrenGio;
    } 
    
    //hiển thị ra kết quả
    document.getElementById('ketQuaViDu4').innerHTML = 'Tiền lương:' + tienLuong.toLocaleString();
}

//Ví dụ 5: Viết chương trình cho phép người dùng nhập vào điểm môn toán lý hóa. Yêu cầu in ra điểm trung bình và xếp loại

document.getElementById('btnTinhDTB').onclick = function() {
    //alert(123);  mục đích test thử

    //input: diemToan, diemLy, diemHoa
    var diemToan = Number(document.getElementById('diemToan').value);
    var diemLy = Number(document.getElementById('diemLy').value);
    var diemHoa = Number(document.getElementById('diemHoa').value);

    //console.log(diemToan,diemLy,diemHoa);

    //output: string
    var ketQua = '';

    //progress

    //bước 1: tính điểm trung bình
    var diemTrungBinh = (diemToan + diemLy + diemHoa) / 3;
    //console.log('diemTrungBinh', diemTrungBinh);
    //bước 2: tính xếp loại
    var xepLoai = '';
    if(diemTrungBinh >= 0 && diemTrungBinh < 5) {
        xepLoai = 'Không đạt';
    }
    else if(diemTrungBinh >= 5 && diemTrungBinh < 8)  {
        xepLoai = 'Đạt';
    }
    else if (diemTrungBinh >= 8 && diemTrungBinh <= 10) {
        xepLoai = 'Giỏi';
    }
    else {
        xepLoai = 'Dữ liệu không hợp lệ';
    }

    //in kết quả ra màn hình
    ketQua = 'Điểm TB: ' + diemTrungBinh + ' - Xếp Loại :' + xepLoai;
    document.getElementById('ketQuaViDu5').innerHTML = ketQua
}

// Ví dụ 6: Tính tiền phạt thẻ tín dụng. 
//Chương trình cho phép nhập số tiền phải trả cho thẻ tín dụng , tiền đã thanh toán 1 phần trong tháng. Tính tiền phạt chưa thanh toán. Giả định lãi suất là 1.5% / tháng.
// trường hợp 1: mượn 5 trả 10 => phạt 0
//trường hợp 2: mượn 10 trả 9 => phạt 1.5% của 1

function inKetQua() {
    var tienMuon = document.getElementById('txt-tien-muon').value*1;
    var tienDaTra = document.getElementById('txt-tien-da-tra').value*1;
    //alert(123);

    if (tienMuon > tienDaTra) {
        var tienPhat = (tienMuon - tienDaTra) * 0.015;
        document.getElementById('result').innerText = `Số tiền phạt của bạn là ${tienPhat}`;
    }

    if (tienMuon <= tienDaTra) {
        document.getElementById('result').innerText = 'Bạn không bị phạt';
    }
}
//Ví dụ 7: Viết chương trình tính toán tiền phải trả theo tuần cho nhân viên dựa vào số giờ làm, tiền theo giờ. Nếu số giờ lownsh ơn 40 giờ 1 tuần thì phải trả giờ OT theo hệ số là 1.5.

const THOI_GIAN_QUY_DINH_MOI_TUAN = 40;
const LUONG_CO_BAN_MOI_GIO = 100;
//const HE_SO_LAM_THEM_GIO = 1.5;

function tinhLuong() {

    //100 nghìn 1h
    // input 36 giờ
    //input 45 giờ
    var soGioLam = document.getElementById('txt-gio-lam').value*1;

    //output
    var tienLuong = 0;

    //progress
    if (soGioLam <= THOI_GIAN_QUY_DINH_MOI_TUAN) {
        tienLuong = soGioLam * LUONG_CO_BAN_MOI_GIO;
    }else {
        tienLuong = soGioLam * LUONG_CO_BAN_MOI_GIO + (soGioLam - THOI_GIAN_QUY_DINH_MOI_TUAN) * LUONG_CO_BAN_MOI_GIO * 1.5;
    }
    
    //in kết quả ra màn hình
    document.getElementById('result2').innerHTML = 'Tổng tiền lương: ' + tienLuong;
}


/* 
ELSE IF
so sánh 2 số a và b
th1: a > b
th2: a < b
th3: a = b
*/
//ví dụ:
// var a = 1, b = 5;
// if (a > b) {
//     console.log('a lớn hơn b');
// }else {
//     console.log('a nhỏ hơn b');
// }else {
//     console.log('a bằng b');
// }

// Ví dụ 8: Viết chương trình vào thông tin 1 sinh viên: toán, lý, hóa.
// Tính và xuất ra kết quả xếp loại theo bảng xếp loại sau: Loại Giỏi:
// điểm TB >= 8.5 <br />
// Loại Khá: 6.5 <= điểm TB < 8.5 <br />
// Loại TB: 5 <= điểm TB < 6.5 <br />
// Loại Yếu: điểm TB < 5

function XepLoai() {
    const diemToan = document.getElementById('txt-toan').value*1;
    const diemLy = document.getElementById('txt-ly').value*1;
    const diemHoa = document.getElementById('txt-hoa').value*1;

    console.log({diemToan, diemLy, diemHoa}); //thử xuất ra màn hình

    var dtb = (diemToan + diemLy + diemHoa) / 3;
    if (dtb >= 8.5) {
        //alert('gioi');
    } else if (6.5 <= dtb && dtb < 8.5) {
        //alert('loai kha');
    } else if (5 <= dtb && dtb < 6.5) {
        //alert('loai TB');
    } else {
        //alert('loai yeu');
    }

    //in kết quả ra màn hình
    //document.getElementById('ketQuaXepLoai').innerHTML = dtb;
}


//SWITCH CASE: đoán được số lượng thì dùng

var number = 10;

switch (number) {
    case 1: {
        console.log('số 1');
        break;  // dùng để dừng lại lựa chọn số nếu đã tìm được rồi
    }
    case 2: {
        console.log('số 2');
        break;
    }
    case 10: {
        console.log('số 10');
        break;
    }
    default :  
        console.log('Giá trị Default');
}

// nếu không tìm được số thì sẽ trả là Giá trị Default