//Bài tập 1: Cho người dùng nhập vào 3 số nguyên. Viết chương trình xuất 3 số theo thứ tự tăng dần.
document.getElementById("btnSapXepTangDan").onclick = function () {
  //input
  var soThuNhat = Number(document.getElementById("soNguyen1").value);
  var soThuHai = Number(document.getElementById("soThu2").value);
  var soThuBa = Number(document.getElementById("soThu3").value);

  //output
  var sapXepSoTangDan = "";

  //progress
  if (soThuNhat > soThuHai && soThuNhat > soThuBa) {
    if (soThuHai > soThuBa) {
      sapXepSoTangDan = soThuBa + "," + soThuHai + "," + soThuNhat;
    } else {
      sapXepSoTangDan = soThuHai + "," + soThuBa + "," + soThuNhat;
    }
  } else if (soThuHai > soThuNhat && soThuHai > soThuBa) {
    if (soThuNhat > soThuBa) {
      sapXepSoTangDan = soThuBa + "," + soThuNhat + "," + soThuHai;
    } else {
      sapXepSoTangDan = soThuNhat + "," + soThuBa + "," + soThuHai;
    }
  } else if (soThuBa > soThuNhat && soThuBa > soThuHai) {
    if (soThuNhat > soThuHai) {
      sapXepSoTangDan = soThuHai + "," + soThuNhat + "," + soThuBa;
    } else {
      sapXepSoTangDan = soThuNhat + "," + soThuHai + "," + soThuBa;
    }
  }
  //in ra màn hinh
  document.getElementById("ketQuaTangDan").innerHTML = sapXepSoTangDan;
};

//Bài tập 2: Viết chương trình “Chào hỏi” các thành viên trong gia đình với các đặc điểm. Đầu tiên máy sẽ hỏi ai sử dụng máy. Sau đó dựa vào câu trả lời và đưa ra lời chào phù hợp. Giả sử trong gia đình có 4 thành viên: Bố (B), Mẹ (M), anh Trai (A) và Em gái (E).
document.getElementById("btnXinChao").onclick = function () {
  //input
  var nguoiSuDung = document.getElementById("chonThanhVien").value;
  //output
  var guiLoiChao = "";
  //progress
  switch (nguoiSuDung) {
    case "B":
      guiLoiChao = "Xin chào Bố";
      break;
    case "M":
      guiLoiChao = "Xin chào Mẹ";
      break;
    case "A":
      guiLoiChao = "Xin chào Anh trai";
      break;
    case "E":
      guiLoiChao = "Xin chào Em gai";
      break;
  }
  //in ra man hinh
  document.getElementById("guiLoiChaoHoi").innerHTML = guiLoiChao;
};

//Bài tập 3: Cho 3 số nguyên. Viết chương trình xuất ra có bao nhiêu số lẻ và bao nhiêu số chẵn.
document.getElementById("btnDemSoChanLe").onclick = function () {
  //input
  var soThu1 = document.getElementById("soNguyenThuNhat").value * 1;
  var soThu2 = Number(document.getElementById("soNguyenThuHai").value);
  var soThu3 = Number(document.getElementById("soNguyenThuBa").value);
  console.log(soThu1);
  console.log(soThu2);
  console.log(soThu3);
  //output
  var demSoChan = 0;
  var demSoLe = 0;
  //progress
  if (soThu1 <= 0 && soThu2 <= 0 && soThu3 <= 0) {
    alert("Số không hợp lệ, vui lòng nhập lại !");
  }
  if (soThu1 % 2 == 0) {
    demSoChan++;
  }
  if (soThu2 % 2 == 0) {
    demSoChan++;
  }
  if (soThu3 % 2 == 0) {
    demSoChan++;
  }
  demSoLe = 3 - demSoChan;
  //in ra man hinh
  document.getElementById("ketQuaDemSo").innerText =
    "Số lượng số chẵn: " + demSoChan + "," + "Số lượng số lẻ: " + demSoLe;
};

//Bài tập 4: Viết chương trình cho nhập 3 cạnh của tam giác. Hãy cho biết đó là tam giác gì? • Ví dụ: a=2, b=2, c=1 => Tam giác cân • a = 3, b=3 c=3 => Tam giác đều • a = 3, b = 4, c=5 => Tam giác vuông (đinh lý Pytago).
document.getElementById("btnDuDoanTamGiac").onclick = function () {
  //input
  var a = Number(document.getElementById("canhThu1").value);
  var b = Number(document.getElementById("canhThu2").value);
  var c = Number(document.getElementById("canhThu3").value);
  //output
  var loaiTamGiac = "";
  //progress
  if (a + b <= c || a + c <= b || b + c <= a) {
    alert("Tam giác không hợp lệ. Xin kiểm tra lại");
  } else {
    if (a == b && b == c) {
      loaiTamGiac = "Tam giác đều";
    } else if (
      a * a + b * b == c * c ||
      a * a + c * c == b * b ||
      b * b + c * c == a * a
    ) {
      loaiTamGiac = "Tam giác vuông";
    } else if (a == b || a == c || b == c) {
      loaiTamGiac = "Tam giác cân";
    } else {
      loaiTamGiac = "Tam giác thường";
    }
  }
  //in ra man hinh
  document.getElementById("ketQuaTamGiac").innerHTML = loaiTamGiac;
};

//BÀI TẬP NÂNG CAO JS BUỔI 4
// Bài tập 2: Viết chương trình nhập vào tháng, năm. Cho biết tháng đó có bao nhiêu ngày (bao gồm tháng của năm nhuận).
document.getElementById("btnTinhNgay").onclick = function () {
  //input
  var nhapThang = Number(document.getElementById("nhapThang").value);
  var nhapNam = Number(document.getElementById("nhapNam").value);
  //output
  var tongSoNgay = 0;
  //progress
  switch (nhapThang) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      tongSoNgay = "31 ngày";
      break;
    //console.log("Month" + nhapThang + "31 ngày");
    case 4:
    case 6:
    case 9:
    case 11:
      tongSoNgay = "30 ngày";
      //console.log("Month" + nhapThang + "30 ngày");
      break;
    case 2:
      if ((nhapNam % 4 == 0 && nhapNam % 100 != 0) || nhapNam % 400 == 0) {
        tongSoNgay = "29 ngày";
      } else {
        tongSoNgay = "28 ngày";
      }
      break;
    default:
      tongSoNgay = "Vui lòng nhập đúng tháng năm";
  }
  document.getElementById("tongSoNgay").innerHTML = tongSoNgay;
};

//Bài tập 3: Viết chương trình nhập vào số nguyên có 3 chữ số. In ra cách đọc nó.
function btnCachDocSo() {
  var n = document.getElementById("soNguyen3ChuSo").value * 1;

  var donVi = n % 10;
  var Chuc = Math.floor((n / 10) % 10);
  var Tram = Math.floor(n / 100);
  
  if (n < 100 || n > 999) {
    alert("Nhập số không hợp lệ, vui lòng nhập lại !");
  } else {
    //in ra hàng trăm
    switch (Tram) {
      case 1:
        Tram = "Một trăm";
        break;
      case 2:
        Tram = "Hai trăm";
        break;
      case 3:
        Tram = "Ba trăm";
        break;
      case 4:
        Tram = "Bốn trăm";
        break;
      case 5:
        Tram = "Năm trăm";
        break;
      case 6:
        Tram = "Sáu trăm";
        break;
      case 7:
        Tram = "Bảy trăm";
        break;
      case 8:
        Tram = "Tám trăm";
        break;
      case 9:
        Tram = "Chín trăm";
        break;
    }
    // in ra chữ "lẻ" nếu hàng chục bằng 0
    if (Chuc % 10 == 0 && donVi != 0) {
      Chuc = "lẻ;"
    }
    //in ra hàng chục
    switch (Chuc) {
      case 1:
        Chuc = "mười";
        break;
      case 2:
        Chuc = "hai mươi";
        break;
      case 3:
        Chuc = "ba mươi";
        break;
      case 4:
        Chuc = "bốn mươi";
        break;
      case 5:
        Chuc = "năm mươi";
        break;
      case 6:
        Chuc = "sáu mươi";
        break;
      case 7:
        Chuc = "bảy mươi";
        break;
      case 8:
        Chuc = "tám mươi";
        break;
      case 9:
        Chuc = "chín mươi";
        break;
    }
    //in ra hàng đơn vị
    switch (donVi) {
      case 1:
        donVi = "một";
        break;
      case 2:
        donVi = "hai";
        break;
      case 3:
        donVi = "ba";
        break;
      case 4:
        donVi = "bốn";
        break;
      case 5:
        donVi = "năm";
        break;
      case 6:
        donVi = "sáu";
        break;
      case 7:
        donVi = "bảy";
        break;
      case 8:
        donVi = "tám";
        break;
      case 9:
        donVi = "chín";
        break;
    }
  }
  document.getElementById("ketQuaCachDocSo").innerHTML = Tram + " " + Chuc + " " + donVi;
}

//Bài tập 4: Cho biết tên và tọa độ nhà của 3 sinh viên. Cho biết tọa độ của trường đại học. Viết chương trình in tên sinh viên xa trường nhất.
document.getElementById("btn-khoang-cach").onclick = function() {
  var tenSV1 = document.getElementById("tenSV1").value;
  var tenSV2 = document.getElementById("tenSV2").value;
  var tenSV3 = document.getElementById("tenSV3").value;
  
  var toaDoX1 = document.getElementById("toaDoX1").value;
  var toaDoX2 = document.getElementById("toaDoX2").value;
  var toaDoX3 = document.getElementById("toaDoX3").value;
  var toaDoX4 = document.getElementById("toaDoX4").value;

  var toaDoY1 = document.getElementById("toaDoY1").value;
  var toaDoY2 = document.getElementById("toaDoY2").value;
  var toaDoY3 = document.getElementById("toaDoY3").value;
  var toaDoY4 = document.getElementById("toaDoY4").value;

  var d1 = Math.sqrt(Math.pow(toaDoX4 - toaDoX1, 2) + Math.pow(toaDoY4 - toaDoY1, 2));

  var d2 = Math.sqrt(Math.pow(toaDoX4 - toaDoX2, 2) + Math.pow(toaDoY4 - toaDoY2, 2));

  var d3 = Math.sqrt(Math.pow(toaDoX4 - toaDoX3, 2) + Math.pow(toaDoY4 - toaDoY3, 2));

  var ketQuaKhoangCach = "";

  if (
    tenSV1 == "" ||
    tenSV2 == "" ||
    tenSV3 == "" ||
    toaDoX1 == "" ||
    toaDoX2 == "" ||
    toaDoX3 == "" ||
    toaDoX4 == "" ||
    toaDoY1 == "" ||
    toaDoY2 =="" ||
    toaDoY3 == "" ||
    toaDoY4 == ""
  ) {
    alert("Nhập không hợp lệ, vui lòng nhập lại !");
  } else {
    if (d1 > d2 && d1 > d3) {
      ketQuaKhoangCach = "Sinh viên xa trường nhất là: " + tenSV1;
    } else if (d2 > d1 && d2 > d3) {
      ketQuaKhoangCach = "Sinh viên xa trường nhất là: " + tenSV2;
    } else {
      ketQuaKhoangCach = "Sinh viên xa trường nhất là: " + tenSV3;
    }
  }
  document.getElementById("xuatRaKetqua").innerHTML = ketQuaKhoangCach;
}