//update hàng loạt nội dung
function sayHello() {
    console.log('chào bạn');
    console.log('chúc bạn ngày tốt lành');
}

//gọi lệnh bao nhiêu lần thì nội dung trên sẽ lặp lại tương ứng, ví dụ ở đây gọi 4 lần
// sayHello();   
// sayHello();
// sayHello();
// sayHello();
//parameter: tham số
function sayHelloByName (username,message) {
    console.log('chào', username);
    console.log(message);
}
//Nam là username, Hello là message => chú ý để đúng thứ tự
sayHelloByName('Nam', 'Hello');
sayHelloByName('Alex', 'Xin chào');

/** */
function tinhDTB(toan, van) {
    var dtb = (toan + van) / 2;
    //return 'Haha';  // nếu không có kết quả sẽ trả về Haha thay vì undefined

    if (dtb == 10) {
        return 'Giỏi quá'
    } else {
        return dtb;
    }
}

tinhDTB(3, 5);  //ket quả là (3 + 5) / 2 = 4
tinhDTB(5, 5);  // ket quả là (5 + 5) / 2 = 5

var dtb1 = tinhDTB(7, 8);
console.log("🚀 ~ file: index.js:35 ~ dtb1", dtb1)

var dtb2 = tinhDTB(10, 10);
console.log("🚀 ~ file: index.js:40 ~ dtb2", dtb2)


// FUNCTION LITERAL

var loGin = function(username, password) {
    console.log('đăng nhập', username, password);
};
loGin('alice', 123456);

//Bài tập 2: Xây dựng chức năng zoom-in và zoom-out cho 2 button bên dưới.

//cach 1: xu ly khai bao nhu binh thuong khi bam nut button
//xu ly Zoom in - phong to
document.getElementById("btnZoomInHome").onclick = function() {
    //input
    var fontSize = document.getElementById("home").style.fontSize;

    var fSize = Number(fontSize.replace("px", ""));
    fSize +=5;   // fSize = fSize + 5

    document.getElementById("home").style.fontSize = fSize + "px";
}
//xu ly Zoom out - thu nho
document.getElementById("btnZoomOutHome").onclick = function() {
    //input
    var fontSize = document.getElementById("home").style.fontSize;

    var fSize = Number(fontSize.replace("px", ""));
    fSize -=5; //fSize = fSize - 5

    document.getElementById("home").style.fontSize = fSize + "px";
}

//cach 2: su dung ham - viết hàm trả về 1 thẻ sau khi DOM
//phóng to button
document.getElementById("btnZoomInHome").onclick = function() {
    // var tagId = domId("home");
    // var fontSize = Number(tagId.style.fontSize.replace("px", ""));
    // tagId.style.fontSize = (fontSize + 5) + "px";

    //cach viet ham rut gon tăng kích thước:
    zoomFontSize(5, "home");
}
//thu nhỏ button
document.getElementById("btnZoomOutHome").onclick = function() {
    // var tagId = domId("home");
    // var fontSize = Number(tagId.style.fontSize.replace("px", ""));
    // tagId.style.fontSize = (fontSize - 5) + "px";

    //cach viet ham rut gon giảm kích thước:
    zoomFontSize(-5, "home");

}

//phong to text
document.getElementById("btnZoomInText").onclick = function() {
    // var tagId = domId("txtContent");
    // var fontSize = Number(tagId.style.fontSize.replace("px", ""));
    // tagId.style.fontSize = (fontSize + 1) + "px";

    //cach viet ham rut gon tăng kích thước:
    zoomFontSize(1, "txtContent");
}
//thu nho text
document.getElementById("btnZoomOutText").onclick = function() {
    // var tagId = domId("txtContent");
    // var fontSize = Number(tagId.style.fontSize.replace("px", ""));
    // tagId.style.fontSize = (fontSize - 1) + "px";

    //cach viet ham rut gon tăng kích thước:
    zoomFontSize(-1, "txtContent");
}
//viết hàm chung để tăng giảm thuộc tính phần tử
function zoomFontSize(size, id) {  //input: size: number ;id: thẻ id
    var tag = domId(id);
    var fontSize = Number(tag.style.fontSize.replace("px", ""));
    fontSize += size;
    tag.style.fontSize = fontSize + "px";   
}

// //viết hàm trả về 1 thẻ sau khi DOM
// function domId(id) {  //id = "acb" | id = "xyz"
//     return document.getElementById(id);
    
// }




