
//viết hàm chung để tăng giảm thuộc tính phần tử
function zoomFontSize(size, id) {  
    //input: size: number ;id: thẻ id
    var tag = domId(id);
    var fontSize = Number(tag.style.fontSize.replace("px", ""));
    fontSize += size;
    tag.style.fontSize = fontSize + "px";
}

//viết hàm trả về 1 thẻ sau khi DOM
function domId(id) {  //id = "acb" | id = "xyz"
    return document.getElementById(id);
    
}




