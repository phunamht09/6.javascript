// BÀI TẬP VỀ NHÀ JS BUỔI 5
//Bài tập 1: Viết chương trình nhập vào thông tin tiêu thụ điện (Tên và số Kw). Tính và xuất Tiền trả theo quy tắc: 50kw đầu - 500d/kw & 50kw kế - 650d/kw & 100kw kế - 850d/kw & 150kw kế - 1100d/kw & Còn lại - 1300kw.

document.getElementById("btnTinhTienDien").onclick = function() {
    //input
    var tenNguoiDung = document.getElementById("tenNguoiDung").value;
    var kw = document.getElementById("nhapKW").value * 1;

    //output
    var tongTien = 0;

    //progress
    if (kw <= 50) {
        tongTien = kw * 500;
    }
    else if (kw <=100) {
        tongTien = 50 * 500 + (kw - 50) * 650;
        console.log("hello");
    }
    else if (kw <= 200) {
        tongTien = 50 * 500 + 50 * 650 + (kw - 100) * 850;
    }
    else if (kw <= 350) {
        tongTien = 50 * 500 + 50 * 650 + 100 * 850 + (kw - 200) * 1100;
    }
    else {
        tongTien = 50 * 500 + 50 * 650 + 100 * 850 + 350 * 1100 + (kw - 350) * 1300;
    }

    //in ra man hinh
    document.getElementById("ketQuaTienDien").innerHTML = " Họ tên: " + tenNguoiDung + " , " + "Tiền điện: " + tongTien.toLocaleString();
}

//Bài tập 1: Viết chương trình nhập vào thông tin của 1 cá nhân (Tên, tổng thu nhập, số người phụ thuộc). Tính và xuất thuế thu nhập ca nhân phải trả theo quy định: Thu nhập chịu thuế = Tổng thu nhập năm - 4 triệu - Số người phụ thuộc*1.6tr.

document.getElementById("btnTinhThue").onclick = function() {
    //input
    var hoTen2 = document.getElementById("hoTen2").value;
    var thuNhapNam = document.getElementById("thuNhapNam").value * 1;
    var phuThuoc = document.getElementById("phuThuoc").value * 1;

    //output
    var thueTNCN = 0;

    //progress
    var thuNhapChiuThue = thuNhapNam - 4000000 - (phuThuoc * 1600000);

    if (thuNhapChiuThue <= 60000000 ) {
        thueTNCN = thuNhapChiuThue * 0.05;
    }
    else if (thuNhapChiuThue <= 120000000) {
        thueTNCN = thuNhapChiuThue * 0.10;
    }
    else if (thuNhapChiuThue <= 210000000) {
        thueTNCN = thuNhapChiuThue * 0.15;
    }
    else if (thuNhapChiuThue <= 384000000) {
        thueTNCN = thuNhapChiuThue * 0.20;
    }
    else if (thuNhapChiuThue <= 624000000) {
        thueTNCN = thuNhapChiuThue * 0.25;
    }
    else if (thuNhapChiuThue <= 960000000) {
        thueTNCN = thuNhapChiuThue * 0.30;
    }
    else {
        thueTNCN = thuNhapChiuThue * 0.35;
    }

    document.getElementById("ketQuaThue").innerHTML = " Họ tên: " + hoTen2 + " , " + "Tiền thuế thu nhập cá nhân: " + thueTNCN.toLocaleString();
}