//ví dụ 1:
var test = document.getElementById('username');
console.log(test);

// Ví dụ 2:
var test1 = document.getElementById("noiDung");
console.log(test1);



//Ví dụ 2: nhập vào sô tiền lương 1h và số giờ làm in ra tổng tiền lương

function tinhTongTien() {
    //alert(123);

    //input: tienLuong1h:number, soGioLam:number

    var tienLuong = document.getElementById('tienLuong1h').value;
    var soGioLam = document.getElementById('soGioLam').value;

    //kiểm tra input
    //console.log('tienLuong1h', tienLuong);
    //console.log('soGioLam', soGioLam);
    //output: tongLuong: number
    //ban đầu chưa nhập thì = 0
    var tongLuong = 0;

    //progress
    tongLuong = tienLuong * soGioLam;

    document.getElementById('tongLuong').innerHTML = tongLuong.toLocaleString();
    //với thẻ đóng mở thì .innerHTML
    //với thẻ input thì .innerText

}

//Ví dụ 3: Xây dựng thông báo form đăng nhập
var btnDangNhap = document.getElementById('btnDangNhap');
btnDangNhap.onclick = function () {
    //alert(123);
    //input: tài khoản: string, mật khẩu: string
    var taiKhoan = document.getElementById('taiKhoan').value;
    var matKhau = document.getElementById('matKhau').value;

    //output: thông báo : string
    var thongBao = '- Tài khoản -' + taiKhoan + '- Mật khẩu -';

    //progress
    var tagKetQua = document.getElementById('ketQuaDangNhap');
    tagKetQua.innerHTML = thongBao;
    //thay đổi màu sắc
    //tagKetQua.style.backgroundColor = 'green';
    //tagKetQua.style.padding = '15px';
    //tagKetQua.style.color = '#fff';
    //tagKetQua.style.margin = '15px';
    tagKetQua.className = 'bg-success p-2 m-2 text-white';
}