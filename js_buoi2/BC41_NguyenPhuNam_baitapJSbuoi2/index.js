//HOÀN THÀNH BÀI TẬP THEO MÔ HÌNH 3 KHỐI

//BÀI TẬP 1: TÍNH TIỀN LƯƠNG NHÂN VIÊN
/** 
INPUT: lương 1 ngày và số ngày làm
PROGRESS: 
bước 1: Khai báo biến lương 1 ngày luong1Ngay và só ngày làm soNgayLam
bước 2: khai báo biến tính tiền lương tinhLuong
bước 3: gán giá trị cho luong1Ngay và soNgayLam
bước 4: tính toán tinhLuong = luong1Ngay * soNgayLam
bước 5: in kết quả ra Console
Output: tiền lương nhân viên theo số ngày làm việc
**/

function tinhTongTien() {
  //input: luong1Ngay:number ; soNgaylam:number
  var luong1Ngay = document.getElementById("luong1Ngay").value;
  var soNgayLam = document.getElementById("soNgayLam").value;
  //output: tongLuong:number
  var tongLuong = 0;

  //progress
  tongLuong = luong1Ngay * soNgayLam;
  document.getElementById("tongLuong").innerHTML = tongLuong.toLocaleString();
}

//BÀI TẬP 2: TÍNH GIÁ TRỊ TRUNG BÌNH
/** 
INPUT: 5 số thực
PROGRESS: 
bước 1: Khai báo biến cho 5 số thực number1, number2, number3, number4, number5
bước 2: khai báo biến giá trị trung bình tinhGiaTriTrungBinh
bước 3: gán giá trị cho 5 số thực number1, number2, number3, number4, number5
bước 4: tính toán tinhGiaTriTrungBinh = (number1 + number2 + number3 + number4 + number5) / 5
bước 5: in kết quả ra Console
Output: giá trị trung bình của 5 số thực
**/
function btnGiaTriTrungBinh() {
  //input: 5 so
  var number1 = (document.getElementById("number1").value);
  var number2 = (document.getElementById("number2").value);
  var number3 = (document.getElementById("number3").value);
  var number4 = (document.getElementById("number4").value);
  var number5 = (document.getElementById("number5").value);

  //output: gia tri trung binh cua 5 so
  var giaTriTrungBinh = 0;

  //progress
  giaTriTrungBinh = (number1 + number2 + number3 + number4 + number5) / 5;

  document.getElementById("ketQua").innerHTML =
    giaTriTrungBinh.toLocaleString();
}

//BÀI TẬP 3: QUY ĐỔI TIỀN
/** 
INPUT: giá trị USD hiện tại
PROGRESS: 
bước 1: Khai báo biến cho giá trị USD hiện tại là giaTriUSD và số lượng quy đổi giaTriNhap
bước 2: khai báo biến quy đổi USD sang VND là quyDoiUSDSangVND
bước 3: gán giá trị cho giá trị USD hiện tại giaTriUSD và số lượng quy đổi giaTriNhap
bước 4: tính toán quyDoiUSDSangVND = giaTriUSD * giaTriNhap
bước 5: in kết quả ra Console
Output: quy đổi USD sang VND
**/
function btnquyDoi() {
    //input
    var tiGiaUSD = document.getElementById('tiGiaUSD').value*1;
    var giaTriNhap = document.getElementById('giaTriNhap').value*1;

    //output
    var doiUSDsangVND = 0;

    //progress
    doiUSDsangVND = tiGiaUSD * giaTriNhap;
    document.getElementById('doiUSDsangVND').innerHTML = doiUSDsangVND.toLocaleString();
}


//BÀI TẬP 4: TÍNH DIỆN TÍCH, CHU VI HÌNH CHỮ NHẬT
/** 
INPUT: chiều dài và chiều rộng hình chữ nhật
PROGRESS: 
bước 1: Khai báo biến cho chiều dài chieuDai và chiều rộng chieuRong
bước 2: khai báo biến cho diện tích dienTich và chu vi là chuVi
bước 3: gán giá trị cho chiều dài chieuDai và chiều rộng chieuRong
bước 4: tính toán diện tích: dienTich = chieuDai * chieuRong và chuVi = (chieuDai + chieuRong) * 2
bước 5: in kết quả ra Console
Output: diện tích và chu vi của hình chữ nhật
**/
function btnTinhToanHCN() {
    //input
    var chieuDai = document.getElementById('chieuDai').value*1;
    var chieuRong = document.getElementById('chieuRong').value*1;

    //output
    var tinhDienTich = 0;
    var tinhChuVi = 0;

    //progress
    tinhDienTich = chieuDai * chieuRong;
    tinhChuVi = (chieuDai + chieuRong) / 2;

    document.getElementById('tinhDienTich').innerHTML = tinhDienTich.toLocaleString();

    document.getElementById('tinhChuVi').innerHTML = tinhChuVi.toLocaleString();

}

//BÀI TẬP 5: TÍNH TỔNG 2 KÝ SỐ
/** 
INPUT: ký số
PROGRESS: 
bước 1: Khai báo biến cho ký số number, donVi, hangChuc, tinhTong
bước 2: gán giá trị cho number
bước 3: tách số hàng đơn vị theo công thức: donVi = number % 10
bước 4: tách số hàng chục theo công thức: hangChuc = Math.floor(number / 10)
bước 5: tính toán tổng 2 ký số: tinhTong = donVi + hangChuc
bước 5: in kết quả ra Console
Output: tổng 2 ký số
**/
function btnTinh2KySo() {
    //input
    var number = document.getElementById('number').value*1;

    //output
    var tongKySo = 0;

    //progress
    var donVi = number % 10;
    var hangChuc = Math.floor(number / 10);
    var tongKySo = donVi + hangChuc;

    document.getElementById('tong2KySo').innerHTML = tongKySo.toLocaleString();
}
