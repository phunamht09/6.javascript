//Mảng - Array: tạo ra nhiều phần tử chỉ với 1 biến

var menu = ["Bún bò","Bún Riêu"];
console.log(menu);

//update hoặc thay thế 1 phần tử có sẵn bằng phần tử khác, ví dụ: update Bún bò thành Phở
menu[0] = "Phở";
console.log(menu);

//thêm 1 phần tử vào danh sách: dùng push
//ví dụ thêm Cơm tấm vào danh sách
menu.push("Cơm tấm");
console.log(menu, menu.length);

//duyệt mảng
for (var i = 0; i < menu.length; i++) {
    var monAn = menu[i];
    console.log(monAn);
}

//duyệt mảng bằng foreach: lấy từng phần tử trong menu truyền vào item
menu.forEach(function (item) {
    console.log("item", item);
});

// forEach ~ high order function: tức là function nằm trong function

//callback function
var introMonAn = function (item) {
    console.log("món ăn: ", item);
}
menu.forEach(introMonAn);

//remove
console.log("before", menu);
// menu.pop();
console.log("after", menu);

// CRUD: Create Read Update Delete

//tìm kiếm vị trí của phần tử
//ví dụ tìm kiếm vị trí của Bún Riêu
var viTri;
for (let index = 0; index < menu.length; index++) {
    var monAn = menu[index];
    if (monAn == "Bún Riêu") {
        viTri = index;
    }
}
console.log(viTri);
//index0f, tìm thấy => trả về index, không tìm thấy trả về -1
viTri = menu.indexOf("Bún Riêu");
console.log(viTri); // 1

// splice: cut các phần tử - vị trí bắt đầu, số lượng
console.log("before splice", menu);
menu.splice(1, 2);
console.log("after splice", menu);

//slice(0, 2) : start, before end => tức là lấy 2 số index là 0, 1 ; không lấy số 2
var cloneMenu = menu.slice(0, 2);
console.log("after slice", cloneMenu);

var colorCar = ["red", "green", "blue"];
var newColorCar = colorCar.map(function (item) {
    return "toyota";
});
console.log("🚀 ~ file: index.js:68 ~ newColorCar ~ newColorCar", newColorCar);

var nums = [3, 2, 3, 5, 6, 9, 10];
var resultArr = nums.filter(function (item) {
    return item < 5;
})
console.log("🚀 ~ file: index.js:74 ~ resultArr ~ resultArr", resultArr)
