var titleEl = document.querySelector(".title");
//querySelector chỉ trả về 1 element đầu tiên khi có nhiều element hợp lệ giống nhau
console.log(titleEl);
//thay đổi màu sắc cho dòng chữ, cũng giống như css
titleEl.style.color = "red";

// var titleEl = document.querySelector("footer .title");
// //querySelector cho thẻ title của footer
// console.log(titleEl);
// titleEl.style.display = "inline"
// titleEl.style.border = "1px solid blue";

var titleList = document.querySelectorAll(".title");
console.log("🚀 ~ file: demo.js:14 ~ titleList", titleList);
//querySelectorAll trả về danh sách, ví dụ ở đây có 3 title bên html, nhưng index sẽ tính từ 0, 1, 2
titleList[0].style.color = "green";
titleList[1].style.color = "blue";

//titleList: danh sách các phần tử
//titleList[0]: 1 phần từ trong danh sách với index=0
//index là số thứ tự, luôn bắt đầu từ 0

console.log(titleList.length);
//length: là số lượng phần tử

//for
for (var i = 0; i < titleList.length; i++) {
    console.log("[i]", i);
    titleList[i].style.color = "blue";
}


