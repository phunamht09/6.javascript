//truyền vào 1 thẻ tr bất kỳ, trả về điểm thẻ tr đó
function getNameFromTrTag(trTag) {
    var tdList = trTag.querySelectorAll("td");
    return tdList[2].innerHTML;
} 

function getScoreFromTrTag(trTag) {
    var tdList = trTag.querySelectorAll("td");
    return tdList[3].innerText * 1;
}


//show thông tin sinh viên giỏi nhất
// lấy danh sách thẻ td chứa điểm
var tdList = document.querySelectorAll(".td-scores");
console.log("🚀 ~ file: main.js:4 ~ tdList", tdList);

var trList = document.querySelectorAll("#tblBody tr");
console.log("🚀 ~ file: main.js:14 ~ trList", trList);

var scoreArr = Array.from(tdList).map(function(tdTag){
    console.log("🚀 ~ file: main.js:7 ~ scoreArr ~ tdTag", tdTag);
    return tdTag.innerHTML * 1;
});

var scoreMax = scoreArr[0];
for (var index = 1; index < scoreArr.length; index++) {
    var current = scoreArr[index];
    if (current > scoreMax) {
        scoreMax = current;
    }
}
console.log(scoreMax); //kết quả: điểm cao nhất là 9.8

//tìm vị trí điểm cao nhất (9.8)
var indexMax = scoreArr.indexOf(scoreMax);
console.log(indexMax); //kết quả: vị trí số 3

//show thông tin
var trMax = trList[indexMax];
var name1 = getNameFromTrTag(trMax);
console.log("🚀 ~ file: main.js:42 ~ name1", name1);

var score1 = getScoreFromTrTag(trMax);
console.log("🚀 ~ file: main.js:45 ~ score1", score1);

document.getElementById("svGioiNhat").innerText = `
// ${getNameFromTrTag(trMax)} - ${getScoreFromTrTag(trMax)}`;

//show số lượng sinh viên giỏi

var sinhVienGioiArr = [];
var listSinhVienTringBinhString = "";

for(var index = 0; index < tdList.length; index++) {
    //trTag: thẻ tr trong mỗi lần lặp
    let trTag = trList[index];

    if(getScoreFromTrTag(trTag)>=8) {
        sinhVienGioiArr.push(trTag);
    }
    if(getScoreFromTrTag(trTag)>=5) {
        var content = `<p class="text-primary"> ${getNameFromTrTag(trTag)} - ${getScoreFromTrTag(trTag)} </p>`;
        listSinhVienTringBinhString += content;
    }
}

document.getElementById("soSVGioi").innerText = sinhVienGioiArr.length;
//console.log(sinhVienGioiArr);

document.getElementById("dsDiemHon5").innerHTML = listSinhVienTringBinhString;
//console.log("🚀 ~ file: main.js:72 ~ listSinhVienTringBinhString", listSinhVienTringBinhString)

//selection, bubble
