//* Array: mảng */
// Khai báo mảng: 1 biến có thể chứa nhiều phần tử
var arrName = ["Nam", "Hằng", "Minh", "Linh", "Hồng"];
{
console.log(arrName);

//Lấy ra 1 giá trị trong mảng
console.log(arrName[2]);  // 2 là index => Minh
console.log(arrName[3]);  // 3 là index => Linh
document.getElementById("hoTen").innerHTML = arrName[4];

//Thay đổi tên có sẵn, ví dụ đổi tên vị trí index số 2 là Minh => sang tên Dung
arrName[2] = "Dung";
console.log(arrName); //kết quả: Nam, Hằng, Dung, Linh, Hồng


//Lấy ra chiều dài (số lượng phần tử của mảng)
console.log("chiều dài mảng : ", arrName.length);

//Cách duyệt mảng: lấy ra/in ra màn hình console của browser
//cách 1: lấy ra từng cái 1 => tuy nhiên nếu nhiều data thì sẽ mất thời gian, ko tối ưu => nên sử dụng vòng lặp theo cách 2
// console.log(arrName[0]);
// console.log(arrName[1]);
// console.log(arrName[2]);
// console.log(arrName[3]);
// console.log(arrName[4]);

//cách 2: sử dụng vòng lặp
var content = "";
for (var index = 0; index < arrName.length; index++) {
    //console.log(arrName[index]);
    content += '<p class="badge badge-success">' + arrName[index] + '</p>' + " ";
}
document.getElementById("content-array").innerHTML = content;   

}
//Hàm thêm 1 giá trị vào mảng có sẵn: push() => giá trị thêm vào sẽ ở cuối mảng

arrName.push("Uyên", "Duy");
console.log(arrName);  //kết quả: Nam, Hằng, Dung, Linh, Hồng, Uyên, Duy

// Hàm thêm 1 hoặc nhiều giá trị vào đầu mảng: unshift() => lúc này vị trí index của các phần tử sẽ bị thay đổi => hạn chế sử dụng
arrName.unshift("Bảo", "An");
console.log(arrName); //kết quả: Bảo, An, Nam, Hằng, Dung, Linh, Hồng, Uyên, Duy

//Hàm xóa giá trị ra khỏi mảng
//splice(): xóa 1 hoặc nhiều giá trị trong mảng => làm thay đổi index và length của mảng
arrName.splice(2,1);  // 2: vị trí index bắt đầu xóa; 1 là xóa chính nó
arrName.splice(2,2);  // 2: vị trí index bắt đầu xóa; 2 là 1 xóa chính nó + 2 giá trị phía sau
console.log(arrName); //=> kết quả: Bảo, An, Linh, Hồng, Uyên, Duy

//Hàm shift(): lấy ra 1 phần tử ở đầu mảng và xóa luôn phần tử đó khỏi mảng (giống như cut trong excel) 

var hoTen1 = arrName.shift();
console.log("giá trị bị lấy đi : ", hoTen1);  // Bảo sẽ bị lấy đi
console.log(arrName);  
//=> kết quả: An, Linh, Hồng, Uyên, Duy

//Hàm pop(): lấy ra 1 phần tử ở cuối mảng và xóa luôn phần tử đó khỏi mảng (giống như cut trong excel)
var hoTen2 = arrName.pop();
console.log("giá trị cuối mảng bị lấy đi : ", hoTen2); // Duy bị lấy đi
console.log(arrName); 
//=> kết quả: An, Linh, Hồng, Uyên


//* DOM : sử dụng tagname (tên thẻ) */

var arrTagSection = document.getElementsByTagName("section");
console.log(arrTagSection);

//muốn đổi tên phần tử (tương tự như array)
arrTagSection[1].innerHTML = "hello cybersoft";
arrTagSection[1].style.color = "yellow";

for(var index = 0; index < arrTagSection.length; index++) {
    arrTagSection[index].className = "badge badge-danger mt-2";
}

//* DOM : sử dụng className (class của thẻ) */
var arrTagClass = document.getElementsByClassName("txt");
console.log(arrTagClass);

for(var index = 0; index < arrTagClass; index++) {
    //mỗi lần duyệt lấy ra 1 tag
    var tag = arrTagClass[index];  //kqua : 0 1 2 3
    tag.className = "txt alert alert-danger";
    tag.innerHTML = "Hello cyber";
}

//* DOM : sử dụng name */
var arrTagName = document.getElementsByName("text-demo");
console.log("arrTagName",arrTagName);

arrTagName[0].style.color = "pink";
for (var index = 0; index < arrTagName.length; index++) {
    //mỗi lần duyệt lấy ra 1 tag
    var tag = arrTagName[index]; // 0 1 2 3
    tag.className = "txt alert alert-danger";
    tag.innerHTML = "hello cafe";
}

//* DOM: qua query selector */
//kết quả trả về thẻ đầu tiên, nếu ko có thì trả về undefined
document.querySelector("#btnSubmit").onclick = function() {
    //alert(123);
    var pText1 = document.querySelector(".p-text").innerHTML;
    alert(pText1);
} 


//* DOM: qua queryslectorAll */
//kết quả trả về là 1 mảng, nếu như 1 phần tử khớp thì vẫn trả về 1 mảng. Nếu ko khớp phần tử nào thì rỗng




