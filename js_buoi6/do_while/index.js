var count = 1000;

//while do: là điều kiện trước rồi mới hành động
while (count <= 10) {
    console.log("yes", count);
    count++;
}

//do while: là hành động trước rồi mới kiểm tra sau
do {
    console.log("yes", count);
    count++;
} while