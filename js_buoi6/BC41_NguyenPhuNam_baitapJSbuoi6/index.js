//Bài tập về nhà JS buổi 6
//Bài tập 1: Tìm số nguyên dương nhỏ nhất sao cho: 1 + 2 +...+ n > 10000.
document.getElementById("btn-tinh-1").onclick = function() {
    var sum = 0;
    var n = 0;
    while (sum < 10000) {
        n++;
        sum = sum + n;
    }
    document.getElementById("ketQua1").innerHTML = n;
    document.getElementById("ketQuaTong").innerHTML = sum;
}

//Bài tập 2: Viết chương trình nhập vào 2 số x, n. Tính tổng S(n) = x + x^2 + x^3 +...+ x^n (sử dụng vòng lặp và hàm).

document.getElementById("btnViDu2").onclick = function() {
    var x = Number(document.getElementById("nhapX").value);
    var n = Number(document.getElementById("nhapN").value);

    //output
    var sum = 0;
    var luyThua = 1;

    var i = 1;

    while (i <= n) {
        luyThua *= x;
        sum += luyThua;
        i++;
    }
    document.getElementById("ketQuaBT2").innerHTML = sum;
}

//Bài tập 3: Nhập vào n. Tính giai thừa 1*2*...*n.
document.getElementById("btnBT3").onclick = function() {
    //input
    var n = Number(document.getElementById("nhapN3").value);
    //output
    var sum = 1;

    var i = 1;
    while (i <= n) {
        sum *= i;
        i++;
    }
    document.getElementById("ketQuaBT3").innerHTML = sum;
}

//Bài tập 4: Hãy viết chương trình khi click vào button sẽ in ra 10 thẻ div. Nếu div nào vị trí chẵn thì background màu đỏ và lẻ thì background màu xanh.
document.getElementById("btnBT4").onclick = function() {
    var output = "";
    var n = 1;

    while (n <= 10) {
        if (n % 2 == 0) {
            var div = '<div class="p-2 w-25 bg-danger text-white">Div chẵn</div>';
            output += div;
            n++;
        } else {
            var div = '<div class="p-2 w-25 bg-primary text-white">Div lẻ</div>';
            output += div;
            n++;
        }
        document.getElementById("ketQuaBT4").innerHTML = output;
    }
}

//Bài tập về nhà thêm JS buổi 6
//Bài tập 5: Viết chương trình có 1 ô input, 1 ô button. Khi click vào button thì in ra các số nguyên tố từ 1 tới giá trị của ô input.
//hàm kiểm tra số nguyên tố
function kiemTraSo(n) {
    var kiemTraSo = true;
    //Nếu n bé hơn 2 tức là không phải số nguyên tố
    if (n < 2) {
        kiemTraSo = false;
    } else if (n == 2) {
        kiemTraSo = true;
    } else if (n % 2 == 0) {
        kiemTraSo = false;
    } else {
        //lặp từ 3 tới n-1 với bước nhảy là 2 (i+=2)
        for (var i = 3; i <= Math.sqrt(n); i += 2) {
            if (n % i == 0) {
                kiemTraSo = false;
                break;
            }
        }
    }
    return kiemTraSo;
}
//in ra cac so nguyen to
document.getElementById("btnBT5").onclick = function() {
    var n = Number(document.getElementById("nhapSo5").value);
    var output = "";
    for (var i = 1; i <= n; i++) {
        if (kiemTraSo(i)) {
            output += i + "  ";
        }
    }
    document.getElementById("ketQuaBT5").innerHTML = output;
}