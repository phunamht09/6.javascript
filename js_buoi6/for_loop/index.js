/**
 * for (khởi tạo ; điều kiện ; bước nhảy)
 * {
 *  hành động
 * }
 * 
 * phân biệt giữa vòng lặp và lần lặp
 * khởi tạo: chỉ được chạy 1 lần duy nhất ở lần lặp đầu tiên
 * 
 * ở lần lặp đầu: khởi tạo => điều kiện => hành động => bước nhảy
 * 
 * ở lần lặp thứ 2 trở đi: điều kiện => hành động => bước nhảy
 */
// for (var i = 0 ; i <= 10 ; i++) {
//     console.log("hello", i);
// }

//Vi du 1: In tat ca so nguyen duong le/ chan  nho hon 100. Dung buoc nhay va dung % 2.
function inKetQuaViDu1() {
    console.log("yes");
    var stringChan = "";
    var stringLe = "";

    for (var i = 0; i<=100; i++) {
        if (i % 2 == 0) {
            stringChan = stringChan + i + "  ";
        } else {
            stringLe = stringLe + i + "  ";
        }
    }
    console.log({stringChan, stringLe});
}

//Ex2
function inKetQuaViDu2() {
    var number = document.getElementById("txt-number-ex2").value*1;
    var sum = 0;
    for (var i = 0; i <= number ; i++) {
        if (i % 2 == 0) {
            sum += i;
        }
    }
    document.getElementById("resultViDu2").innerText = sum;
}
