function inKetQua() {
    var number = document.getElementById("txt-number").value * 1;
    var sum = 0;
    var count = 1;

    do {
        sum += count;
        count++;
    } while (count <= number);
    document.getElementById("result").innerText = sum;
}