//Bài tập về nhà JS buổi 6
//Bài tập 1: Tìm số nguyên dương nhỏ nhất sao cho: 1 + 2 +...+ n > 10000.
document.getElementById("btn-tinh-1").onclick = function() {
    var sum = 0;
    var n = 0;
    while (sum < 10000) {
        n++;
        sum = sum + n;
    }
    document.getElementById("ketQua1").innerHTML = n;
    document.getElementById("ketQuaTong").innerHTML = sum;
}

//Bài tập 2: Viết chương trình nhập vào 2 số x, n. Tính tổng S(n) = x + x^2 + x^3 +...+ x^n (sử dụng vòng lặp và hàm).

document.getElementById("btnViDu2").onclick = function() {
    var x = Number(document.getElementById("nhapX").value);
    var n = Number(document.getElementById("nhapN").value);

    //output
    var sum = 0;
    var luyThua = 1;

    var i = 1;

    while (i <= n) {
        luyThua *= x;
        sum += luyThua;
        i++;
    }
    document.getElementById("ketQuaBT2").innerHTML = sum;
}

//Bài tập 3: Nhập vào n. Tính giai thừa 1*2*...*n.
document.getElementById("btnBT3").onclick = function() {
    //input
    var n = Number(document.getElementById("nhapN3").value);
    //output
    var sum = 1;

    var i = 1;
    while (i <= n) {
        sum *= i;
        i++;
    }
    document.getElementById("ketQuaBT3").innerHTML = sum;
}

//Bài tập 4: Hãy viết chương trình khi click vào button sẽ in ra 10 thẻ div. Nếu div nào vị trí chẵn thì background màu đỏ và lẻ thì background màu xanh.
document.getElementById("btnBT4").onclick = function() {
    var output = "";
    var n = 1;

    while (n <= 10) {
        if (n % 2 == 0) {
            var div = '<div class="p-2 w-25 bg-danger text-white">Div chẵn</div>';
            output += div;
            n++;
        } else {
            var div = '<div class="p-2 w-25 bg-primary text-white">Div lẻ</div>';
            output += div;
            n++;
        }
        document.getElementById("ketQuaBT4").innerHTML = output;
    }
}

//Bài tập về nhà thêm JS buổi 6
//Bài tập 5: Viết chương trình có 1 ô input, 1 ô button. Khi click vào button thì in ra các số nguyên tố từ 1 tới giá trị của ô input.
//hàm kiểm tra số nguyên tố
function kiemTraSo(n) {
    var kiemTraSo = true;
    //Nếu n bé hơn 2 tức là không phải số nguyên tố
    if (n < 2) {
        kiemTraSo = false;
    } else if (n == 2) {
        kiemTraSo = true;
    } else if (n % 2 == 0) {
        kiemTraSo = false;
    } else {
        //lặp từ 3 tới n-1 với bước nhảy là 2 (i+=2)
        for (var i = 3; i <= Math.sqrt(n); i += 2) {
            if (n % i == 0) {
                kiemTraSo = false;
                break;
            }
        }
    }
    return kiemTraSo;
}
//in ra cac so nguyen to
document.getElementById("btnBT5").onclick = function() {
    var n = Number(document.getElementById("nhapSo5").value);
    var output = "";
    for (var i = 1; i <= n; i++) {
        if (kiemTraSo(i)) {
            output += i + "  ";
        }
    }
    document.getElementById("ketQuaBT5").innerHTML = output;
}

//Bài tập 6: Cho phép người dùng nhập vào 1 số, cho biết số đó có phải số nguyên tố hay không?.
document.getElementById("btnKiemTra").onclick = function() {
    //input: number
    var number = Number(document.getElementById("nhapSo6").value);

    //output
    var output = "";

    //kiem tra so nguyen to
    var kiemTraSNT = true;
    var soHang = 2;

    while(soHang <= Math.sqrt(number)) { //việc dùng căn bậc 2 cũng giúp giảm số hạng cần phải xét
        //nếu chia hết cho thêm 1 số nào nữa (tức là ước > 2)
        if (number  % soHang === 0) {
            kiemTraSNT = false;
            break; //thoát ra khỏi vòng lặp nếu false, tránh mất thời gian phải xét các bước nhảy tiếp theo
        }
        soHang++;
    }
    if (kiemTraSNT) {  //kiemTraSNT = true
        output = number + " là số nguyên tố";
    } else {
        output = number + " không là số nguyên tố";
    }
    //in kết quả
    document.getElementById("ketQua6").innerHTML = output;
}

//Bài tập 7: Viết chương trình cho phép người dùng nhập vào 1 số in ra số ngôi sao tương ứng.
document.getElementById("btnBT7").onclick = function() {
    //input: number
    var number = Number(document.getElementById("nhapSo7").value);
    //output: string
    var output = "";

    //cách 1: sử dụng vòng lặp while
    // //bước 1: khai báo biến thay đổi
    // var soSao = 0;
    
    // //bước 2: xử lý điều kiện lặp
    // while (soSao < number) {
    //     output = output + " * ";
    //     soSao++;
    // }

    //cách 2: sử dụng vòng lặp for
    for (var soSao = 0; soSao < number; soSao++) {
        output = output + " * ";
    }
    document.getElementById("ketQuaBT7").innerHTML = output;
}

//Bài tập 8: Viết chương trình cho người dùng nhập vào số hàng và số cột in ra các ngôi sao.
document.getElementById("btnBT8").onclick = function() {
    //input: number
    var soHang = Number(document.getElementById("soHang").value);
    var soCot = Number(document.getElementById("soCot").value);
    //output: string
    var output = "";

    for (var i = 1; i <= soHang; i++) {
        //cách 1: vòng lặp lồng trong vòng lặp
        //code để tạo thêm 1 hàng
        // for (var soSao = 1; soSao <= soCot; soSao++) {
        //     output += " * ";
        // }
        // output += "<br/>";


        //cách 2: khai báo theo hàm, hàm này tạo ngoài nút click button
        var htmlHangSao = in1HangSao(soCot);
        output += htmlHangSao + "<br/>";
    }
    document.getElementById("ketQua8").innerHTML = output;
}
//khai báo hàm
function in1HangSao(soSao) { //input
    var output = "";
    for (var i = 1; i <= soSao; i++) {
        output += " * ";
    }
    return output; //output
}

// Bài tập 9: Cho phép người dùng nhập vào 1 số. In ra các số nguyên tố nhỏ hơn hoặc bằng số người dùng nhập.

document.getElementById("btnBT9").onclick = function() {
    //input: number
    var number = Number(document.getElementById("nhap9").value);
    //output
    var ketQua = "";

    for (var iSo = 2; iSo <= number; iSo++) {
        //chạy qua từ 2->n kiểm tra từng số có phải là số nguyên tố hay không dựa vào hàm đã xay dựng
        var checkSNT = kiemTraSoNguyenTo(iSo);
        //nếu true => iSo sẽ là số nguyên tố
        if (checkSNT) {
            ketQua += iSo + " ";
        }
    }
    document.getElementById("ketQua9").innerHTML = ketQua;
}

//khai báo hàm kiểm tra xem có phải số nguyên tố hay không
function kiemTraSoNguyenTo(number) { //input : number
    //output: true hoặc false
    var checkSNT = true;
    for(var i = 2; i <= Math.sqrt(number); i++) {
        if(number % i === 0) {
            checkSNT = false;
            break;
        }
    }
    return checkSNT;
}
