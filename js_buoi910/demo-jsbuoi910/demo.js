//Object

var cat1 = {
    name: "mun",
    age: 2,
    speak: function() {
        console.log("meo meo", this.name); //this là gọi cùng cấp
    },
};
//var variable = {
//key:value
//}
//array => index ~ có thứ tự
//object => key ~ ko có thứ tự
//truy xuất value qua key

var catName = cat1.name;

//update
cat1.name = "bull";
console.log(cat1);
cat1.speak();

//var catAge = cat1.age
var catAge = cat1["age"];

var key = "age";
var value = "20";
cat1[key] = value;
console.log("🚀 ~ file: demo.js:28 ~ cat1", cat1)


//pass by value là là string, number, boolean
//pass by reference là array, object

var a=2;
var b=a;
b = 5;
console.log(a);

var cat2 = cat1;
console.log("🚀 ~ file: demo.js:41 ~ cat1", cat1)
console.log("🚀 ~ file: demo.js:41 ~ cat2", cat2);

//update tên cho cat2
cat2.name = "Mực";
console.log("🚀 ~ file: demo.js:41 ~ cat1", cat1);
console.log("🚀 ~ file: demo.js:46 ~ cat2", cat2);


