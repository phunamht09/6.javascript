// BÀI TẬP: tính giá trị a, b, c:
var a, b, c;

a = 10;
a +=a;
// a = a + a;
//a=10+10=20
console.log(a); //a=20

b = ++a + 5; // b=26 a=21
c = a++ + 5; // c=26 a=22

console.log(a); // a=22

a = 0;
console.log(b);  //b=26
console.log(c); //c=26


//++a + b: thì sẽ ưu tiên cộng giá trị a trước rồi sau đó mới cộng b
//a++ + b: thì lấy giá trị của a + b luôn; sau đó a = a+1
//=> chú ý JS sẽ update lấy giá trị cuối cùng để tính toán
