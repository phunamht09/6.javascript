//BÀI TẬP: Viết chương trình tính chiều dài cạnh huyền của một tam giác vuông khi biết 2 cạnh góc vuông (trang 24 tài liệu js buổi 1)

/**
Đầu vào:
- 2 cạng góc vuông (3cm, 4cm);

Các bước xử lý:
bước 1:...
bước 2:...

Đầu ra:
- Cạnh vuông: 5cm
*/

var edge1 = 3;
var edge2 = 4;

var edge3 = edge1 * edge1 + edge2 * edge2;

edge3 = Math.sqrt(edge3);
console.log(edge3);

//BÀI TẬP: TÍNH TỔNG KÝ SỐ
/**
 * input: 246
 * 
 * 
 *output = 12

 */
var number = 246;
var donVi = number % 10; //ket qua: 6
var hangChuc = Math.floor(number / 10) % 10;  //ket qua : 4
var hangTram = Math.floor(number / 100);  //ket qua: 2
// Math.floor(6.86) => 6
var tinhTong = donVi + hangChuc + hangTram;

 console.log(tinhTong);