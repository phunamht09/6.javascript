console.log("Hello");

// variable

var username = "Bob";

// Bob là value
// username là biến

console.log("username");
console.log("username1");
console.log("username2");

// data type ~ kiểu dữ liệu
// Tom là string
var cat = "Tom";

// number - có nhiều giá trị
var age = "3";

// boolean ~ có 2 giá trị là true / false
var isLogin = true;

//null - chỉ có 1 giá trị là null, bây giờ ko có nhưng sau này có thể có
var isMarried = null;

//undefined - không có, không xác định
var is_married = undefined;

//có 2 cách đặt tên là camel và snake case => chỉ dùng 1 trong 2
//camel case
var isMarried = null;

//snak case
var is_married = undefined;

// gán lại giá trị
var isHoliday = true;
console.log("isHoliday", isHoliday);
isHoliday = false;
console.log("isHoliday: ", isHoliday);

// toán tử: operator //
var num1 = 2;
var num2 = 3;
var num3 = num1 + num2;
var num4 = 1;
num4 = num4 + 1;
//-
var num5 = 2 * 2;
//4
var num6 = 6 / 3;
//2

var num7 = 21 % 5;

var num8 = 5;
//num8 = num8 + 1;

//num8++;
//++num8;
var num9 = num8++ + 2;

var num10 = 10;
var num11 = num10++ + 

console.log(`__file: index.js__line 35__num3`, num3);



var sohang1 = 5;
var sohang2 = 10;
var tong = sohang1 + sohang2;
var tich = sohang1 * sohang2;
console.log("tong=", tong);
console.log("tich=", tich);

//demo toán tử tăng giảm biến://
//x++; giống x = x + 1; giống x +=1 =>tăng thêm 1
//x--; giống x = x - 1; giống x -=1 =>giảm bớt 1
//x +=y; giống x = x + y;
// x *=y giống x = x*y;
// x /=y giống x = x/y;

//HẰNG SỐ - CONSTANT//
//giá trị duy nhất không thay đổi, nếu thay đổi sẽ báo lỗi
const HANG_SO_LUONG = 5;
// HE_SO_LUONG = 10; hệ thống sẽ báo lỗi

// MÔ HINH 3 KHỐI//
//Ví dụ về tính lương nhân viên làm trên 1 tháng (28 ngày)/ Lương cơ bản là 20$ 1 ngày
//Input: đầu vào là lương 1 ngày và số ngày làm
var luong1ngay = 20;
var songaylam = 28;

//Output: tính lương trên số ngày làm việc
var luong = 0;

//progress: xử lý
luong = luong1ngay * songaylam;

//output ra màn hình
console.log("tong luong", luong);

// TRUY XUẤT THÔNG QUA ID //
var tagH3 = document.getElementById('title');

//.innerHTML: nội dung ở giữa 2 thẻ html đóng mở => thay đổi nội dung có sẵn
tagH3.innerHTML = "Cybersoft.edu.vn";

// Ví dụ 2: về id input
//.value: phần nội dung của thẻ input thường chứa giá trị người dùng nhập vào
var tagInput = document.getElementById('txt');
tagInput.value = 'Hello moi người';



//Ví dụ 3: về người dùng nhập và hiển thị kết quả ra màn hình khi nhấn nút

function hienThiThongTin () {
  //Lấy giá trị khi gọi hàm hiển thị 

  //input: string
  var input = document.getElementById('giaTriNhap');
  console.log(input.value);

  //output: string
  var output = '';

  //progress
  output = input.value

  //Xử lý kết quả hiển thị lên giao diện
  var tagSpanKetQua = document.getElementById('KetQuaHienThi');
  tagSpanKetQua.innerHTML = output;

}


// Ví dụ 4: Nhập số tiền lương 1h và số giờ làm In ra tổng lương bằng số giờ nhân tiền lương

function tinhTongLuong() {
  //input: tienLuong1h:number, soGioLam:number
  var tienLuong = document.getElementById("tienLuong1h").value;
  var soGioLam = document.getElementById("soGioLam").value;

  //kiểm tra input
  //console.log('tienLuong', tienLuong);
  //console.log('soGioLam', soGioLam);
  //output: tongLuong:number
  var tongLuong = 0;
  //progress:
  tongLuong = tienLuong * soGioLam;

  document.getElementById("tongLuong").innerHTML = tongLuong;
}
